﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColourChangeDev : MonoBehaviour
{
    //method declared with the same name as the door script
    public void Operate()
    {
        //RGB values range 0-1
        Color random = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f));
        //Colour is set to the material 
        GetComponent<Renderer>().material.color = random;
    }
}
