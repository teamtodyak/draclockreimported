﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeviceOperator : MonoBehaviour
{
    //Distance between the player and devices 
    public float radius = 0.5f;

     void Update()
    {
        if (Input.GetButtonDown ("Fire3"))
        {
            //Respond to the input button
            Collider[] hitColliders =
                //OverlapSphere() return a list of nearby objects
                Physics.OverlapSphere(transform.position, radius);
            foreach (Collider hitCollider in hitColliders)
            {
                //SendMessage() tries to call the named function, regardless of the target's type
                hitCollider.SendMessage("Operate", SendMessageOptions.DontRequireReceiver);
            }
        }
    }
}
