﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeviceTrigger : MonoBehaviour
{
    //list of target objects that this trigger will activate
    [SerializeField] private GameObject[] targets;

    //OnTriggerEnter() is called when another objects enters the trigger volume
    private void OnTriggerEnter(Collider other)
    {
        foreach (GameObject target in targets)
        {
            target.SendMessage("Activate");
        }
    }

    //is called when an object leaves the trigger volume
  /* private void OnTriggerExit(Collider other)
    {
        foreach (GameObject target in targets)
        {
            target.SendMessage("Deactivate");
        }
    }*/

}
