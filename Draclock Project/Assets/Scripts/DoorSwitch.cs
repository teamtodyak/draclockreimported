﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorSwitch : MonoBehaviour
{
    public AudioClip MusicClip;

    public AudioSource MusicSource;

    //Position to offset when the door opens
    [SerializeField] private Vector3 dPos;
    //Boolean to keep on track the door open or close stare
    private bool _open;
    // Open or close the door depeding on the state.

    private void Start()
    {
        MusicSource.clip = MusicClip;
    }
    public void Operate()
    {
        if (_open)
        {
            Vector3 pos = transform.position - dPos;
            transform.position = pos;
       
        }
        else
        {
            Vector3 pos = transform.position + dPos;
            transform.position = pos;
            MusicSource.Play();
            Debug.Log("The sound should be playing rn");
        }
       // _open = !_open;
    }

}