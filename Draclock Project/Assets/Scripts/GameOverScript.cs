﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOverScript : MonoBehaviour
{
    public bool IsGameOver;
    public GameObject Player;
    public GameObject Canvas;
    public GameObject Camera;
    // Start is called before the first frame update
    void Start()
    {
        this.gameObject.transform.parent = null;
    }

    // Update is called once per frame
    void Update()
    {
        if (IsGameOver)
        {
            Camera.transform.parent = null;
            Destroy(Player);
            this.transform.SetParent(Canvas.transform);
        }
    }
}
