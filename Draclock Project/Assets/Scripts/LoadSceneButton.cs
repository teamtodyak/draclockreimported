﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class LoadSceneButton : MonoBehaviour
{
    public Button yourButton;
    public int SceneIndexNum;
    void Start()
    {
        Button btn = yourButton;
        btn.onClick.AddListener(TaskOnClick);
        Debug.Log("Start worked");
    }

    void TaskOnClick()
    {
        SceneManager.LoadScene(SceneIndexNum);
        Debug.Log("Button has been clicked");
    }
}