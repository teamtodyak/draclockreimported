﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PickupCounter : MonoBehaviour
{
    public int count;
    private TextMeshProUGUI textmesh;
    void Start()
    {
        textmesh = this.gameObject.GetComponent<TextMeshProUGUI>();
    }

    // Update is called once per frame
    void Update()
    {
        textmesh.text = string.Format("x {0}", count.ToString());
    }
}
