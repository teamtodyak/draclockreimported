﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlatformStick : MonoBehaviour
{
    public GameObject Jumps;
    public GameObject Visions;
    public GameObject GO;
    public GameObject TimerObject;
    public AudioClip DieSound;
    public AudioClip PickupSound;
    public AudioSource MusicSource;
    public AudioSource PickupSource;

    private void Start()
    {
        MusicSource.clip = DieSound;
        PickupSource.clip = PickupSound;
    }

    private void OnTriggerEnter(Collider hit)
    {
        if (hit.gameObject.name == "MovingPlatform")
        {

            this.transform.parent = hit.gameObject.transform;

        }
        else if (hit.gameObject.name != "Player")
        {
            this.transform.parent = null;
        }

        if (hit.gameObject.name == "JumpPickup")
        {
            Destroy(hit.gameObject);
            Jumps.GetComponent<PickupCounter>().count = Jumps.GetComponent<PickupCounter>().count + 1;
            PickupSource.Play();
        }

        if (hit.gameObject.name == "VisionPickup")
        {
            Destroy(hit.gameObject);
            Visions.GetComponent<PickupCounter>().count = Visions.GetComponent<PickupCounter>().count + 1;
            PickupSource.Play();
        }
        
        if (hit.gameObject.name == "Kill"){
            GO.GetComponent<GameOverScript>().IsGameOver = true;
            MusicSource.Play();

        }

        if (hit.gameObject.name == "FinishLine")
        {
            SceneManager.LoadScene("Level1", LoadSceneMode.Single);
        }

        if (hit.gameObject.name == "Regret")
        {
            TimerObject.GetComponent<TimerScript>().TimeInSeconds = TimerObject.GetComponent<TimerScript>().TimeInSeconds + 15;
            Destroy(hit.gameObject);
            PickupSource.Play();

        }
        Debug.Log(hit.gameObject.name);
    }

    private void OnTriggerExit(Collider hit)
    {
       
    }

}
