﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TimerScript : MonoBehaviour
{
    public float TimeInSeconds;
    public GameObject GO;
    private float startTime;
    private TextMeshProUGUI textmesh;
    // Start is called before the first frame update
    void Start()
    {
        startTime = Time.time;
        textmesh = this.gameObject.GetComponent<TextMeshProUGUI>();

    }

    // Update is called once per frame
    void Update()
    {
        float t = Time.time - startTime;
        float TimeLeft = TimeInSeconds - t;
        string seconds;
        //Debug.Log(TimeLeft);
        if (TimeLeft <= 0)
        {
            GO.GetComponent<GameOverScript>().IsGameOver = true;
        } else {
            if (Mathf.Floor(TimeLeft % 60) < 10)
                {
                    seconds = string.Format("0{0}", (Mathf.Floor(TimeLeft % 60)).ToString());
                }
            else
                {
                    seconds = (Mathf.Floor(TimeLeft % 60)).ToString();
                }
            textmesh.text = string.Format("{0}:{1}", (Mathf.Floor((TimeLeft / 60) % 60)).ToString(), seconds);
        }
    }
}
